require emu-plc, 0.1.+
require emu-motor, 0.1.+
require singlemotion,1.4.6+



# Set environmental variables
epicsEnvSet("ASYN_PORT",        "GEOBRICK_ASYN")
epicsEnvSet("PMAC_IP", 	        "10.10.3.42") 
epicsEnvSet("PMAC_PORT",        "1025")


# fonctions from TPMAC
# Connection to GEOBRICK, create a asyn port
pmacAsynIPConfigure($(ASYN_PORT), $(PMAC_IP):$(PMAC_PORT))

# s7plcConfigure (PLCname, IPaddr, port, inSize, outSize, bigEndian, recvTimeout, sendIntervall)
s7plcConfigure ("plc", "10.10.2.3", 2000, 40, 6, 1, 1000, 500)


# i/o PLC
dbLoadRecords("emu_input.db")
dbLoadRecords("emu_output.db")

# load PMAC (geobrick) database
dbLoadRecords("emu_get_value_pmac.db")
dbLoadRecords("emu_set_value_pmac.db")
dbLoadRecords("emu_console.db")

#communication PLC PMAC
dbLoadRecords("emu_set_bo_PLC.db")
dbLoadRecords("emu_set_bo_pmac.db")

